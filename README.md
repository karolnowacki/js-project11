# Project 11 - Języki Symboliczne
## Program w Pythonie wywołujący funkcję z biblioteki dll/so liczącą iloczyn macierzy

Repozytorium GIT: https://bitbucket.org/karolnowacki/js-project11/src

Autor: Karol Nowacki D/125774

## Opis zadania

* Okno z przyciskiem "Uruchom obliczenia" wraz z polami pozwalającymi wybrać:
    - liczbę kolumn macierzy
    - liczbę wierszy macierzy
    - zakres generownych wartości
    - rodzaj generownych wartości (liczby całkowite/rzeczywiste)
    - liczbę powtórzeń mnożenia (do uśrwdnienia czasu obliczeń)
* Okno z informacjami o ostatnim uruchomieniu
  * czas obliczeń w Pythonie
  * czas obliczeń w C/C++
  * czas potrzebny na konwersję danych
* Okno z możliwości ręcznego prowadzenia macierzy
* Po wciśnięciu przycisku "Uruchom obliczenia" generowane są dwie macierze o podanych przez użytkonika parametrach, które są następnie mnożone w Pythonie oraz w C/C++ (kod mnożący wywołany z pliku .dll/.so). Jeśli macierzy nie da się pomnożyć, ma zostać wyświetlony odpowiedni komunikat w oknie dialogowym (proszę użyć mechanizmu wyjątków)
* Implememtacja w Pythonie i w C lub C++ mają być zrealizowane w dwóch klasach (`MnozenieMacierzyCpp`, `MnozenieMacierzyPython`) dziedziczących po klasie MnożenieMacierzy z metodą `mnoz`.
* Zapisywany jest czas obliczeń w pierwszym i drugim języku oraz czas przygotowania danych do przesłania do C/C++
* Wyniki wypisywane są do okna z informacjami
* Do wywyłania funkcji z dll/so należy stosować bibliotekę [ctypes](https://docs.python.org/3/library/ctypes.html)

## Testy
1. Wykonanie mnożenia (w Python i przez dll/so):

<!--
\left[\begin{array}{cc}1 & 0 \\0 & 1\end{array}\right]
\times \left[\begin{array}{cc}2 & 3 \\4 & 5\end{array}\right]
= \left[\begin{array}{cc}2 & 3 \\4 & 5\end{array}\right]
-->

![Test1Latex](https://latex.codecogs.com/svg.image?%5Cleft%5B%5Cbegin%7Barray%7D%7Bcc%7D1%20&%200%20%5C%5C0%20&%201%5Cend%7Barray%7D%5Cright%5D%5Ctimes%20%5Cleft%5B%5Cbegin%7Barray%7D%7Bcc%7D2%20&%203%20%5C%5C4%20&%205%5Cend%7Barray%7D%5Cright%5D=%20%5Cleft%5B%5Cbegin%7Barray%7D%7Bcc%7D2%20&%203%20%5C%5C4%20&%205%5Cend%7Barray%7D%5Cright%5D)

2. Wykonanie mnożenia (w Python i przez dll/so):

<!--
\left[\begin{array}{ccc}1 & 2 & 1\\4 & -2 & 1\end{array}\right]
\times
\left[\begin{array}{cc}0 & 1\\2 & 0\\-1 & 1\end{array}\right]= \left[\begin{array}{cc}3 & 2\\-4 & 4\\ \end{array}\right]
-->

![Test2Latex](https://latex.codecogs.com/svg.image?%5Cleft%5B%5Cbegin%7Barray%7D%7Bccc%7D1%20&%202%20&%201%5C%5C4%20&%20-2%20&%201%5Cend%7Barray%7D%5Cright%5D%5Ctimes%20%5Cleft%5B%5Cbegin%7Barray%7D%7Bcc%7D0%20&%201%5C%5C2%20&%200%5C%5C-1%20&%201%5Cend%7Barray%7D%5Cright%5D=%20%5Cleft%5B%5Cbegin%7Barray%7D%7Bcc%7D3%20&%202%5C%5C-4%20&%204%5C%5C%20%5Cend%7Barray%7D%5Cright%5D)

3. Wykonanie mnożenia (w Python i przez dll/so):

<!--
\left[\begin{array}{cc}1 & 2 \\ 4 & -2\\ 0 & 3\end{array}\right]
\times
\left[\begin{array}{ccc}0 & 1 & -3\\2 & -1 & 0\end{array}\right]
=
\left[\begin{array}{ccc}4 & -1 & -3\\-4 & 6 & -12\\6 & -3 & 0\end{array}\right]
-->

![Test3Latex](https://latex.codecogs.com/svg.image?%5Cleft%5B%5Cbegin%7Barray%7D%7Bcc%7D1%20&%202%20%5C%5C%204%20&%20-2%5C%5C%200%20&%203%5Cend%7Barray%7D%5Cright%5D%5Ctimes%5Cleft%5B%5Cbegin%7Barray%7D%7Bccc%7D0%20&%201%20&%20-3%5C%5C2%20&%20-1%20&%200%5Cend%7Barray%7D%5Cright%5D=%20%5Cleft%5B%5Cbegin%7Barray%7D%7Bccc%7D4%20&%20-1%20&%20-3%5C%5C-4%20&%206%20&%20-12%5C%5C6%20&%20-3%20&%200%5Cend%7Barray%7D%5Cright%5D)

4. Próba wykonania mnożenia macierzy 2x3 przez macierz 1x2 (oczekiwany komunikat o błędzie i możliwość kontynuowania przecy z programem bez ponownego uruchomiania)

5. Uzyskanie porównania czasów dla mnożenia macieży 1x1, 10x10 i 50x50 (odpowiednio 100000, 1000 i 50 powtórzeń, liczby całkowite)

6. Uzyskanie porównania czasów dla mnożenia macierzy 200x200 (3 powtórzenia, liczby rzeczywiste)
