from ctypes import *
import os

class MnozenieMacierzySizeException(Exception):
    pass

class MnozenieMacierzy():

    def __init__(self, A, B):
        self.A = A
        self.B = B

    def mnoz(self):
        pass

class MnozenieMacierzyCpp(MnozenieMacierzy):
    def __init__(self, A, B):
        super().__init__(A,B)
        self.lib = cdll.LoadLibrary(os.path.dirname(__file__)+"/../clib/libmatrix.so")
        self.mulD = self.lib.matrixMulD
        self.mulD.restype = POINTER(c_double)
        self.mulL = self.lib.matrixMulL
        self.mulL.restype = POINTER(c_longlong)

    def matrixPy2CD(self, m):
        aType = c_double * int(len(m) * len(m[0]))
        a = list()
        for i in m:
            a.extend(i)
        return aType(*a)
    def matrixC2Py(self, m):
        c = len(self.B[0])
        r = len(self.A)
        return [[ m[j*c + i] for i in range(c)] for j in range(r)]
    def matrixPy2CL(self, m):
        aType = c_longlong * int(len(m) * len(m[0]))
        a = list()
        for i in m:
            a.extend(i)
        return aType(*a)
    def mnoz(self):
        if len(self.A[0]) != len(self.B):
            raise MnozenieMacierzySizeException()
        func = self.mulD
        conv = self.matrixPy2CD
        if isinstance(self.A[0][0], int) and isinstance(self.B[0][0], int):
            func = self.mulL
            conv = self.matrixPy2CL
        return self.matrixC2Py(func(
            conv(self.A),
            conv(self.B),
            c_int(len(self.A)), c_int(len(self.A[0])),
            c_int(len(self.B)), c_int(len(self.B[0]))))


class MnozenieMacierzyPython(MnozenieMacierzy):
    def mnoz(self):
        if len(self.A[0]) != len(self.B):
            raise MnozenieMacierzySizeException()
        C = [[0 for i in range(len(self.B[0]))] for j in range(len(self.A))]
        for i in range(len(self.A)):
            for j in range(len(self.B[0])):
                for k in range(len(self.B)):
                    C[i][j] += self.A[i][k] * self.B[k][j]
        return C
