import random

def matrixGenerator(type, col, row, min, max):
    col = int(col)
    row = int(row)
    assert col > 0, "Liczba kolumn powinna być większa od zera"
    assert row > 0, "Liczba wierszy powinna być większa od zara"
    assert min < max, "min powinno byc mniejsze od max"
    genFunc = None
    if type == "int":
        genFunc = random.randint
    if type == "float":
        genFunc = random.uniform

    return [[ genFunc(min,max) for i in range(col)] for j in range(row)]
