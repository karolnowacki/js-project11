import wx
import wx.grid

class matrixWindow(wx.Frame):

    def __init__(self, *args, **kw):
        super(matrixWindow, self).__init__(*args, **kw, style = wx.DEFAULT_FRAME_STYLE)
        self.callback = None

    def InitUI(self):
        pn = wx.Panel(self)

        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add((-1, 10), 0)

        self.grid = wx.grid.Grid(pn)
        self.grid.CreateGrid(self.settings['row'], self.settings['col'])

        vbox.Add(self.grid, 1, wx.EXPAND | wx.ALL, 9)

        btnOk = wx.Button(pn, wx.ID_ANY, 'Ok')
        self.Bind(wx.EVT_BUTTON, self.OnClose, id=btnOk.GetId())
        vbox.Add(btnOk, flag=wx.EXPAND, border=10)

        pn.SetSizer(vbox)

    def setSettings(self, settings):
        self.settings = settings

    def setMatrix(self,m):
        for i in range(len(m)):
            for j in range(len(m[0])):
                self.grid.SetCellValue(i, j, str(m[i][j]))

    def setCallback(self,f):
        self.callback = f

    def OnClose(self, event):
        if callable(self.callback):
            m = [[ 0.0 for j in range(self.settings['col'])] for i in range(self.settings['row'])]
            for i in range(self.settings['row']):
                for j in range(self.settings['col']):
                    try:
                        m[i][j] = float(self.grid.GetCellValue(i,j))
                    except:
                        pass

            self.callback(m)

        self.Close()
