import wx

import modules.matrixWindow
import modules.helpers
import modules.MnozenieMacierzy
import timeit

class mainWindow(wx.Frame):

    def __init__(self, *args, **kw):
        super(mainWindow, self).__init__(*args, **kw, style = wx.DEFAULT_FRAME_STYLE & ~(wx.RESIZE_BORDER | wx.MAXIMIZE_BOX))

        self.InitUI()
        self.matrixA = [[]]
        self.matrixB = [[]]
        self.resultC = [[]]
        self.resultP = [[]]

    def InitUI(self):
        pn = wx.Panel(self)

        wx.StaticBox(pn, label='Macierz A', pos=(5, 5), size=(195, 170))
        wx.StaticText(pn, label='Wiersze:', pos=(15, 30))
        self.rowA = wx.SpinCtrl(pn, value='3', pos=(75, 27), size=(120, -1), min=1, max=1000)
        wx.StaticText(pn, label='Kolumny:', pos=(15, 65))
        self.colA = wx.SpinCtrl(pn, value='3', pos=(75, 62), size=(120, -1), min=1, max=1000)
        self.rndA = wx.CheckBox(pn, label='Losuj wartości', pos=(15, 100))
        self.rndA.SetValue(True)
        self.setABtn = wx.Button(pn, label='Ustaw wartosci', pos=(15, 130))

        self.setABtn.Bind(wx.EVT_BUTTON, self.onSetMatrixA)

        bshift = 220
        wx.StaticBox(pn, label='Macierz B', pos=(bshift+5, 5), size=(195, 170))
        wx.StaticText(pn, label='Wiersze:', pos=(bshift+15, 30))
        self.rowB = wx.SpinCtrl(pn, value='3', pos=(bshift+75, 27), size=(120, -1), min=1, max=1000)
        wx.StaticText(pn, label='Kolumny:', pos=(bshift+15, 65))
        self.colB = wx.SpinCtrl(pn, value='3', pos=(bshift+75, 62), size=(120, -1), min=1, max=1000)
        self.rndB = wx.CheckBox(pn, label='Losuj wartości', pos=(bshift+15, 100))
        self.rndB.SetValue(True)
        self.setBBtn = wx.Button(pn, label='Ustaw wartosci', pos=(bshift+15, 130))

        self.setBBtn.Bind(wx.EVT_BUTTON, self.onSetMatrixB)

        self.colA.Bind( wx.EVT_SPINCTRL, self.onColRowChange )
        self.rowB.Bind( wx.EVT_SPINCTRL, self.onColRowChange )

        wx.StaticBox(pn, label='Generator wartości', pos=(5, 180), size=(250, 150))
        wx.StaticText(pn, label='Min:', pos=(15, 210))
        self.genMin = wx.SpinCtrlDouble(pn, value='-10', pos=(45, 207), size=(180, -1), min=-10000, max=10000)
        self.genMin.SetDigits(3)
        wx.StaticText(pn, label='Max:', pos=(15, 250))
        self.genMax = wx.SpinCtrlDouble(pn, value='10', pos=(45, 247), size=(180, -1), min=-10000, max=10000)
        self.genMax.SetDigits(3)
        self.rb1 = wx.RadioButton(pn, label='Float', pos=(15, 290))
        self.rb1.SetValue(True)
        self.rb2 = wx.RadioButton(pn, label='Integer', pos=(120, 290))
        self.rb1.Bind(wx.EVT_RADIOBUTTON, self.genTypeChange)
        self.rb2.Bind(wx.EVT_RADIOBUTTON, self.genTypeChange)

        wx.StaticText(pn, label='Powtórz obliczenia', pos=(15, 347))
        self.repeat = wx.SpinCtrl(pn, value='1', pos=(160, 345), size=(160, -1), min=1, max=100000)
        self.goBtn = wx.Button(pn, label='Uruchom obliczenia', pos=(140, 400))
        self.goBtn.Bind(wx.EVT_BUTTON, self.onRunCalculations)

        self.SetSize((440, 500))
        self.SetTitle('Project 11 - Karol Nowacki')
        self.Centre()
        self.Show(True)

    def genTypeChange(self, e):
        if self.rb1.GetValue():
            self.genMin.SetDigits(3)
            self.genMax.SetDigits(3)
        if self.rb2.GetValue():
            self.genMin.SetDigits(0)
            self.genMax.SetDigits(0)

    def onColRowChange(self, e):
        val = e.GetEventObject().GetValue()
        self.colA.SetValue(val)
        self.rowB.SetValue(val)
    def getSettings(self):
        ret = {
            'A': {
                'row' : int(self.rowA.GetValue()),
                'col' : int(self.colA.GetValue()),
                'random' : bool(self.rndA.GetValue())
            },
            'B': {
                'row' : int(self.rowB.GetValue()),
                'col' : int(self.colB.GetValue()),
                'random' : bool(self.rndB.GetValue())
            },
            'gen': {
                'type' : 'float',
                'min' : float(self.genMin.GetValue()),
                'max' : float(self.genMax.GetValue())
            },
            'repeat' : int(self.repeat.GetValue())
        }
        if self.rb2.GetValue():
            ret['gen']['type'] = 'int'
            ret['gen']['min'] = int(self.genMin.GetValue())
            ret['gen']['max'] = int(self.genMax.GetValue())
        return ret
    def onSetMatrixA(self, e):
        self.rndA.SetValue(False)
        mw = modules.matrixWindow.matrixWindow(self)
        mw.setSettings(self.getSettings()['A'])
        mw.setCallback(self.setMatrixA)
        mw.InitUI()
        mw.SetTitle('Macierz A')
        mw.setMatrix(self.matrixA)
        mw.Show()
    def setMatrixA(self,m):
        self.matrixA = m
        print(m)
    def onSetMatrixB(self, e):
        self.rndB.SetValue(False)
        mw = modules.matrixWindow.matrixWindow(self)
        mw.setSettings(self.getSettings()['B'])
        mw.setCallback(self.setMatrixB)
        mw.InitUI()
        mw.SetTitle('Macierz B')
        mw.setMatrix(self.matrixB)
        mw.Show()
    def setMatrixB(self,m):
        self.matrixB = m
        print(m)
    def onRunCalculations(self, e):
        settings = self.getSettings()
        dialog = wx.ProgressDialog("Mnożenie Macierzy", "Przygotowanie", settings['repeat'],
            style=wx.PD_CAN_ABORT | wx.PD_ELAPSED_TIME)
        keepGoing = True

        if settings['A']['random']:
            self.matrixA = modules.helpers.matrixGenerator(
                settings['gen']['type'],
                settings['A']['col'],
                settings['A']['row'],
                settings['gen']['min'],
                settings['gen']['max'])
        if settings['B']['random']:
            self.matrixB = modules.helpers.matrixGenerator(
                settings['gen']['type'],
                settings['B']['col'],
                settings['B']['row'],
                settings['gen']['min'],
                settings['gen']['max'])
        try:
            count = 0
            mul = modules.MnozenieMacierzy.MnozenieMacierzyCpp(self.matrixA, self.matrixB)
            keepGoing = dialog.Update(count, "Mnożenie przy użyciu C")
            starttimeC = timeit.default_timer()
            endtimeC = starttimeC
            while keepGoing and count < settings['repeat']:
                count = count + 1
                self.resultC = mul.mnoz()
                keepGoing = dialog.Update(count)
            endtimeC = timeit.default_timer();

            mw = modules.matrixWindow.matrixWindow(self)
            mw.setSettings({'row' : len(self.resultC), 'col' : len(self.resultC[0])})
            mw.InitUI()
            mw.SetTitle('Wynik mnożenia w C')
            mw.setMatrix(self.resultC)
            mw.Show()


            count = 0
            mul = modules.MnozenieMacierzy.MnozenieMacierzyPython(self.matrixA, self.matrixB)
            keepGoing = dialog.Update(count, "Mnożenie przy użyciu Python")
            starttimeP = timeit.default_timer();
            endtimeP = starttimeP
            while keepGoing and count < settings['repeat']:
                count = count + 1
                self.resultP = mul.mnoz()
                keepGoing = dialog.Update(count)
            endtimeP = timeit.default_timer();

            mw = modules.matrixWindow.matrixWindow(self)
            mw.setSettings({'row' : len(self.resultP), 'col' : len(self.resultP[0])})
            mw.InitUI()
            mw.SetTitle('Wynik mnożenia w Python')
            mw.setMatrix(self.resultP)
            mw.Show()
        except modules.MnozenieMacierzy.MnozenieMacierzySizeException:
            dial = wx.MessageDialog(self, "Niewłaściwe rozmiary macieży", "Error", wx.OK|wx.STAY_ON_TOP|wx.CENTRE)
            dial.ShowModal()
        except Exception as e:
            dial = wx.MessageDialog(self, str(e), "Error", wx.OK|wx.STAY_ON_TOP|wx.CENTRE)
            dial.ShowModal()
        else:
            dial = wx.MessageDialog(self,
                "Czas liczenia w C: {:f}\nCzas liczenia w Pythonie: {:f}".format(
                    (endtimeC-starttimeC), (endtimeP-starttimeP)),
                "Wyniki", wx.OK|wx.STAY_ON_TOP|wx.CENTRE)
            dial.ShowModal()

        dialog.Destroy()


def run():
    app = wx.App()
    window = mainWindow(None)
    window.Show()
    app.MainLoop()
