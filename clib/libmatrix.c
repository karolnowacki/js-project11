#include "libmatrix.h"

#include<stdio.h>

double * matrixMulD(double *A, double *B, int rowA, int colA, int rowB, int colB) {
  double * C;
  int i,j,k;

  if (colA != rowB)
    return NULL;

  C = malloc(rowA * colB * sizeof(C));
  memset(C, 0, rowA * colB * sizeof(*C));

  for (i=0; i<rowA; ++i)
    for (j=0; j<colB; ++j)
      for (k=0; k<rowB; ++k)
        C[i*colB + j] += A[i * rowB + k] * B[k * colB +j];

  return C;

}

long long * matrixMulL(long long *A, long long *B, int rowA, int colA, int rowB, int colB) {
  long long * C;
  int i,j,k;

  if (colA != rowB)
    return NULL;

  C = malloc(rowA * colB * sizeof(C));
  memset(C, 0, rowA * colB * sizeof(*C));

  for (i=0; i<rowA; ++i)
    for (j=0; j<colB; ++j)
      for (k=0; k<rowB; ++k)
        C[i*colB + j] += A[i * rowB + k] * B[k * colB +j];

  return C;

}
