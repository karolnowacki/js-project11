import unittest

import os
import sys

sys.path.append(os.path.dirname(__file__) + '/../modules')
import MnozenieMacierzy as mm

class TestMul():

    def compare(self, a, b):
        assert(len(a) == len(b))
        for ar,br in zip(a,b):
            assert(len(ar) == len(br))
            for ai,bi in zip(ar,br):
                assert(ai == bi)

    def checkMul(self, a, b, r):
        self.A = a
        self.B = b
        ret = self.mnoz()
        assert(ret == r)

        if isinstance(self.A[0][0], int) and isinstance(self.B[0][0], int):
            self.A = [[float(x) for x in row] for row in a]
            self.B = [[float(x) for x in row] for row in b]
            r = [[float(x) for x in row] for row in r]
            ret = self.mnoz()
            assert(ret == r)


    def test_zad1(self):
        self.checkMul(
            [[1,0],[0,1]],
            [[2,3],[4,5]],
            [[2,3],[4,5]]
        )

    def test_zad2(self):
        self.checkMul(
            [[1,2,1],[4,-2,0]],
            [[0,1],[2,0],[-1,1]],
            [[3,2],[-4,4]]
        )

    def test_zad3(self):
        self.checkMul(
            [[1,2],[4,-2],[0,3]],
            [[0,1,-3],[2,-1,0]],
            [[4,-1,-3],[-4,6,-12],[6,-3,0]]
        )

    def test_zad4(self):
        self.assertRaises(mm.MnozenieMacierzySizeException,
            self.checkMul,
                [[1,2],[3,4],[5,6]],
                [[1,2]],
                []
        )

    def test_size1(self):
        self.checkMul(
            [[1,1],[1,1],[1,1],[1,1]],
            [[1,2,3], [3,4,5]],
            [[4,6,8],[4,6,8],[4,6,8],[4,6,8]]
        )


class TestMulPython(unittest.TestCase, mm.MnozenieMacierzyPython, TestMul):
    pass

class TestMulCpp(unittest.TestCase, mm.MnozenieMacierzyCpp, TestMul):
    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        mm.MnozenieMacierzyCpp.__init__(self, None, None)




if __name__ == "__main__":
    unittest.main()
