import unittest

import os
import sys

import timeit

sys.path.append(os.path.dirname(__file__) + '/../modules')
import MnozenieMacierzy as mm
import helpers

class PerformaceMul(unittest.TestCase):

    def test_zad5(self):
        for size, reps in [ (1,100000), (10, 1000), (50, 50) ]:
            with self.subTest(size = "{}x{}".format(size,size), reps=reps):
                mc = mm.MnozenieMacierzyCpp(
                    helpers.matrixGenerator("int", size, size, -100, 100),
                    helpers.matrixGenerator("int", size, size, -100, 100),
                )
                mp = mm.MnozenieMacierzyPython(
                    helpers.matrixGenerator("int", size, size, -100, 100),
                    helpers.matrixGenerator("int", size, size, -100, 100),
                )

                print("\nCzas mnożenia macierzy %dx%d %d powrórzeń - losowe wartosci int" % (size, size, reps))
                tmc = timeit.timeit(lambda: mc.mnoz(), number=reps)
                print("C\t- %f" % ( tmc ))
                tmp = timeit.timeit(lambda: mp.mnoz(), number=reps)
                print("Python\t- %f" % ( tmp ))

    def test_zad6(self):
        mc = mm.MnozenieMacierzyCpp(
            helpers.matrixGenerator("float", 200, 200, -100, 100),
            helpers.matrixGenerator("float", 200, 200, -100, 100),
        )
        mp = mm.MnozenieMacierzyPython(
            helpers.matrixGenerator("float", 200, 200, -100, 100),
            helpers.matrixGenerator("float", 200, 200, -100, 100),
        )
        print("\nCzas mnożenia macierzy 200x200 3 powrórzenia - losowe wartosci float")
        tmc = timeit.timeit(lambda: mc.mnoz(), number=3)
        print("C\t- %f" % ( tmc ))
        tmp = timeit.timeit(lambda: mp.mnoz(), number=3)
        print("Python\t- %f" % ( tmp ))

if __name__ == "__main__":
    unittest.main()
