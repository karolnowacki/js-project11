PYTHON = python3

.PHONY: all
all: libs tests

libs:
	$(MAKE) -C clib

tests: tests/*_tests.py
	for file in $^; do echo $${f}; $(PYTHON) $${file}; done
